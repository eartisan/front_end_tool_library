import { createStore } from "vuex";
import common from "./modules/common";
import user from "./modules/user";

const store = createStore({});

store.registerModule("common", common);
store.registerModule("user", user);

// 定义 state|modules|dispatch|commit 的参数类型
declare module "vuex" {
  type StoreStateType = typeof store.state;
  type ModulesType = {
    common: typeof common.state;
    user: typeof user.state;
  };
  type DispatchKeys = string;
  type CommitKeys = string;
}

export default store;
