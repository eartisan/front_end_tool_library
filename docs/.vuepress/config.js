module.exports = {
  title: '一匠前端',
  base: '/front_end_tool_library/',
  // 设置网站图标
  head: [
    ['link', { rel: 'shortcut icon', type: "image/x-icon", href: `/favicon.ico` }]
  ],
  themeConfig: {
    nav: [
      { text: '规范文档', link: '/guide' },
      { text: '知识扩展', link: '/extends' },
      {
        text: '站外链接', items: [
          { text: 'Gitee', link: 'https://gitee.com/eartisan/front_end_tool_library' },
          { text: 'TypeScript', link: 'https://www.tslang.cn/' },
          { text: 'UniApp', link: 'https://uniapp.dcloud.io/' },
          { text: 'Vue2.0', link: 'https://cn.vuejs.org/' },
          { text: 'Vue3.0', link: 'https://v3.cn.vuejs.org/' },
          { text: `Mr.Mao's blog`, link: 'https://tuimao233.gitee.io/mao-blog/' },
        ]
      },
      { text: '常见问题', link: '/problem' },
    ]
  }
}
