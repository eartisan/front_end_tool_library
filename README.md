## 前端工具库

项目工具库包括项目配置，讨论文章内容/技术讨论，工具类方法，项目模板。每个文件目录都有详细的说明。用于搭设公司基础技术建设，业务和技术组件的复用，项目模板的复用，项目规范管理。最主要的目的，减少团队成员的工作量，提高代码质量。

线上文档：https://eartisan.gitee.io/front_end_tool_library/

## 项目目录说明

```text
├── discussion-articles  # 技术讨论记录
├── docs # 线上文档源码
├── options  # 项目配置文件
├── project-template # 公司项目模板
├── utils-library
|   ├── components # 复用组件库
|   ├── utils # 复用工具库
├── ...
└── README.md # 说明文件
```

