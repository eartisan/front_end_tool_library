---
sidebar: auto
title: '知识扩展'
---
## 开发框架

Vue2.0：[https://cn.vuejs.org/](https://cn.vuejs.org/)

Vue3.0：[https://v3.cn.vuejs.org/](https://v3.cn.vuejs.org/)

VueRouter：[https://router.vuejs.org/zh/](https://router.vuejs.org/zh/)

Vuex：[https://vuex.vuejs.org/zh/](https://vuex.vuejs.org/zh/)

Vuex(数据持久化)：[https://github.com/robinvdvleuten/vuex-persistedstate](https://github.com/robinvdvleuten/vuex-persistedstate)

Nuxt：[https://zh.nuxtjs.org/](https://zh.nuxtjs.org/)

Lavas：[https://lavas.baidu.com/](https://lavas.baidu.com/)

UniApp：[https://uniapp.dcloud.io/](https://uniapp.dcloud.io/)

uniSimpleRouter：[http://hhyang.cn/src/router/start/applets/explian.html](http://hhyang.cn/src/router/start/applets/explian.html)

## UI组件库

Vuetify：[https://vuetifyjs.com/zh-Hans/](https://vuetifyjs.com/zh-Hans/)

Element：[https://element.eleme.cn/#/zh-CN](https://element.eleme.cn/#/zh-CN)

Ant Design of Vue：[https://www.antdv.com/docs/vue/introduce-cn/](https://www.antdv.com/docs/vue/introduce-cn/)

IView：[https://www.iviewui.com/](https://www.iviewui.com/)

Vant：[https://youzan.github.io/vant/#/zh-CN/](https://youzan.github.io/vant/#/zh-CN/)

Mint：[http://mint-ui.github.io/#!/zh-cn](http://mint-ui.github.io/#!/zh-cn)

UView：[https://uviewui.com/](https://uviewui.com/)

## 开发工具

lodash：[https://www.lodashjs.com/](https://www.lodashjs.com/)

qrcode：[https://www.npmjs.com/package/qrcodejs2](https://www.npmjs.com/package/qrcodejs2)

jquery：[https://jquery.cuishifeng.cn/](https://jquery.cuishifeng.cn/)

## 时间处理

Moment(已放弃维护)：[http://momentjs.cn/](http://momentjs.cn/)

Dayjs(推荐)：[https://day.js.org/](https://day.js.org/)

Luxon：[https://moment.github.io/luxon/](https://moment.github.io/luxon/)

DateFns：[https://date-fns.org/docs/](https://date-fns.org/docs/)

## 移动端调试
[https://github.com/Tencent/vConsole/blob/dev/README_CN.md](https://github.com/Tencent/vConsole/blob/dev/README_CN.md)

## 数据可视化

Echarts(通用)：[https://echarts.apache.org/zh/builder.html](https://echarts.apache.org/zh/builder.html)

Highcharts(兼容性强)：[https://www.highcharts.com.cn/](https://www.highcharts.com.cn/)

D3(功能强大)：[https://d3js.org/](https://d3js.org/)

## 独立UI组件

dplayer：[http://dplayer.js.org/](http://dplayer.js.org/)

swiper：[https://www.swiper.com.cn/](https://www.swiper.com.cn/)

better-scroll：[https://github.com/ustbhuangyi/better-scroll](https://github.com/ustbhuangyi/better-scroll)

signature：[https://github.com/szimek/signature_pad](https://github.com/szimek/signature_pad)