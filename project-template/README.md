#### 项目模板说明

~~~
├── alpha@preset-vue-next-cli  # web-vue@3 页面cli开发模板(内部测试版)
├── alpha@preset-vue-next-vite  # web-vue@3 页面vite开发模板(内部测试版)
├── preset-uni@full  # uni-app 多端开发模板(正式版)
├── preset-uni-js@full # uni-app 多端开发模板(正式版)
├── preset-vue@full  # web-vue@2 页面开发模板(正式版)
├── preset-vue-self@full  # web-vue@2-self 页面响应式模板(正式版)
~~~

#### uni-app 项目模板使用说明

1. 创建并进入项目文件夹`SVN`拷贝项目文件：（注意，拷贝完毕后删除.svn文件，避免影响整体项目模板）

   ~~~
   svn://gitee.com/eartisan/front_end_tool_library/project-template/preset-uni@full
   ~~~

2. 进入项目安装依赖：`npm install`

3. 运行打包命令：`npm run dev:mp-weixin`

#### web-vue@2 与其他项目模板使用说明

1. 创建并进入项目文件夹`SVN`拷贝项目文件：（注意，拷贝完毕后删除.svn文件，避免影响整体项目模板）

   ~~~
   web-vue2(web模板): svn://gitee.com/eartisan/front_end_tool_library/project-template/preset-vue@full
   
   web-vue2-self(web自适应模板): svn://gitee.com/eartisan/front_end_tool_library/project-template/preset-vue-self@full
   
   web-vue3-cli(vue3-cli测试模板): svn://gitee.com/eartisan/front_end_tool_library/project-template/alpha@preset-vue-next-cli
   
   web-vue3-vite(vue3-vite测试模板): svn://gitee.com/eartisan/front_end_tool_library/project-template/alpha@preset-vue-next-vite
   ~~~

2. 进入项目安装依赖：`npm install`

3. 运行打包命令：`npm run serve`

