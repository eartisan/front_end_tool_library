/*
 * @Author: 毛先生
 * @Date: 2020-08-18 14:29:19
 * @LastEditTime: 2020-08-18 14:46:47
 * @LastEditors: 毛先生
 * @Description:
 * @傻瓜都能写出计算机能理解的程序。优秀的程序员写出的是人类能读懂的代码。
 */
interface wxQueryOps {
  /** 选择器名称 */
  selectStr: string;
  nodeRefs: [keyof UniApp.SelectorQuery];
  fieldsOptions?: UniApp.NodeField;
  selectorQuery?: keyof {
    select: string;
    selectAll: string;
  };
  filter?: (res: any) => void;
}
interface wxQueryRes {
  boundingClientRect: UniApp.NodeInfo;
  scrollOffset: UniApp.NodeInfo;
  fields: UniApp.NodeInfo;
  context: UniApp.NodeInfo;
}
declare const wxQuery = (): Promise<wxQueryRes> => {
  console.log("--");
};
export default wxQuery;
