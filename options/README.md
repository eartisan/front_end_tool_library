# 1. 配置文件说明

配置用于项目配置，适用于web项目与uniapp项目使用。

## vsCode 安装扩展

安装ESLint 、Vetur插件

![](images/ESLint-extend.jpg)

![](images/Vetur-extend.jpg)

## vsCode 进行全局配置

ctrl + shift + p 输入 `>Open Settings (json)`，将以下代码添加到json中。

~~~js
{
  // 保存时调用eslint的自动修复
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  // 调用规则适用于
  "eslint.validate": [
    "javascirpt",
    "typescript"
  ]
}
~~~
# 2. 使用项目模板

使用项目模板，可直接跳过以下环节，且功能更加齐全，具体参考该项目目录。
https://gitee.com/eartisan/front_end_tool_library/tree/master/project-template

# 3. 在Vue项目中使用

**一、在`vue create [项目名]` 创建流程中选择...**

~~~makefile
? Check the features needed for your project: # 选择配置项, <空格>表示选择, <a>表示全选, <i>表示反转
  #...
  (*) TypeScript		# .ts的解析器
  (*) Linter / Formatter		# 代码风格检查和格式化
  #...

#↓↓↓...↓↓↓#

? Pick a linter / formatter config: # 选择Eslint 代码验证规则 (通常Prettier用的比较多)
> ESLint + Prettier

#↓↓↓...↓↓↓#
~~~

**二、将前端工具库/options中的.eslintrc.js，.prettierrc复制至项目根文件**

![](images/vue-options-1.jpg)》》》》》![](images/vue-options-2.jpg)

# 4. 在UniApp中使用

**一、通过 CLI 创建 uni-app 项目，选择TS模板**

~~~
vue create -p dcloudio/uni-preset-vue [项目名称]
~~~

**二、安装，使用ESLint+Prettier**

~~~
cnpm i eslint eslint-plugin-prettier @vue/cli-plugin-eslint eslint-plugin-vue prettier @vue/eslint-config-prettier @vue/eslint-config-typescript @typescript-eslint/eslint-plugin @typescript-eslint/parser -D
~~~

**三、将前端工具库/options中的.vscode，.eslintrc.js，.prettierrc复制至项目根文件**

![](images/uni-options-1.jpg)》》》》》![](images/uni-options-2.jpg)