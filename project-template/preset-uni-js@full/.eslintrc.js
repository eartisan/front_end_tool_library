module.exports = {
  globals: {},
  extends: [
    'plugin:vue/essential',
    'eslint:recommended',
    '@vue/prettier',
    'eslint-config-craftsman/uni-javascript',
  ],
};
