/* 语法糖工具类 */

// for in封装
export function forIn(
  object,
  callback
) {
  for (const key in object) {
    callback(key, object[key]);
  }
  return object;
}
