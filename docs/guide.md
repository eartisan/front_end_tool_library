---
sidebar: auto
title: '一匠科技前端规范(v0.5)'
---

## 项目构建说明

前端统一 IDE 使用vscode，项目代码检测全由eslint，prettier进行代码校验，配合vscode进行动态修复，遇到 eslint 报错请不要轻易修改 eslint 规则，请咨询相关人员进行解决。

vscode 必装插件：ESLint，Vetur，Chinese，create-uniapp-view。

- 前端主体技术栈为：[TypeScript](https://www.tslang.cn/)，[Vue2.0](https://cn.vuejs.org/)，[UniApp](https://uniapp.dcloud.io/)，Vue2.0用于开发webPC或移动端web项目。UniApp用于开发小程序，移动端应用，开发小程序不使用原生小程序进行开发。无特殊情况不允许使用主体技术栈以外的技术进行开发。
- 开发中可使用UI组件库：[Vant](https://youzan.github.io/vant/#/zh-CN/)（web移动端，小程序），[Element](https://element.eleme.cn/#/zh-CN)（webPC），[bootstrap-vue](http://code.z01.com/bootstrap-vue/)（webPC），[Ant Design of Vue](https://www.antdv.com/docs/vue/introduce-cn/)（webPC），[uView](https://uviewui.com/)（多端开发）
- 无特殊情况，使用项目模板（指定主体技术栈）进行开发。项目要求组件化开发，结构化，清晰化项目结构。 请遵循所定义的规范进行开发。

## CSS/SCSS 规范

### Class命名
类命名采用小写，中划线（ - ）分开关键字命名，英文单词尽量不要缩写。
~~~css
/* 符合 */
.video-warp {}
/* 不符合 */
.video_warp {}
.videoWarp {}
.VIDEOWarp {}
~~~

### 全局样式（主体色）
所有主题色号不允许在单独组件使用，只允许使用src/style中存在的变量/集合。
其余如字体字号，全局尺寸，请酝酿使用。
~~~text
...
├── src
│ 	└── style
│		├── class.scss (全局class)
│		├── mixin.scss (全局集合方法)
│ 		└── variables.scss (全局样式变量)
....
~~~

### 避免事项
- 避免大量的嵌套规则。当可读性受到影响时，将之打断。推荐避免出现多于10行以上的嵌套规则出现

- 少用#，少用*，少用标签选择器，!important 尽量避免使用 。

### CSS 属性书写顺序

样式编写，建议遵循以下顺序。

1. 布局定位属性：`display / position / float / clear / visibility / overflow/...`
2. 自身属性：`width / height / margin / padding / border / background /...`
3. 文本属性：`color / font / text-decoration / text-align / vertical-align /...`
4. 其他属性（CSS3）：`content / cursor / border-radius / box-shadow / text-shadow / background:linear-gradient/...`

## JavaScript 规范

### 变量命名

- 变量命名方法为小驼峰命名法，前缀应为名词（userInfo，shopList，naviList.....）

~~~js
data: () => ({
  // 错误
  user_info: {/*...*/},
  ShopList: {/*...*/},
  naViList: {/*...*/},

  // 正确
  userInfo: {/*...*/},
  shopList: {/*...*/},
  naviList: {/*...*/},
})
~~~

- 函数命名方法为小驼峰命名法，前缀应为动词（getUserInfo，setShopList，initNaviList.....）

~~~js
methods: {
  // 错误
  GetUserInfo(){/*...*/},
  set_shopList(){/*...*/},
  initnavilist(){/*...*/},

  // 正确
  getUserInfo(){/*...*/},
  setShopList(){/*...*/},
  initNaviList(){/*...*/},
}
~~~

- 常量（不会再次进行修改）命名则采用大写加下划线命名（RECEIVE_USER_INFO）

~~~js
const RECEIVE_USER_INFO = 'receive_user_info'
~~~

### 变量引用
- 对所有引用都使用 const，不要使用 var
~~~js
// 错误
var a = 1
var b = 2

// 正确
const a = 1
const b = 2
~~~
- 如果引用是可变动的，则使用 let
~~~js
// 错误
var count = 1
if (count < 10) {
  count += 1
}

// 正确
let count = 1
if (count < 10) {
  count += 1
}
~~~

### 模块化开发
采用模块化进行开发，使用标准的 ES6 模块语法 import 和 export
~~~ts
// 错误
const util = require('./util')
module.exports = util

// 正确
export const paramsAnaly = (url: string) => {/*...*/}
//...
import { paramsAnaly } from "@/utils"
~~~

### Promise
使用async/await代替.then
~~~js
// 错误
const makeRequest = () => {
  return promise1()
    .then(value1 => {
      return promise2(value1)
        .then(value2 => {       
          return promise3(value1, value2)
        })
    })
}

// 正确
const makeRequest = async () => {
  const value1 = await promise1()
  const value2 = await promise2(value1)
  return promise3(value1, value2)
}
~~~

### if ... else
尽量减少代码嵌套
~~~js
// 错误
() => {
  if(isLogin) {
    if(isDelete) {
      // 代码....
    }
  }
}

// 正确
() => {
  if(!isLogin) {
    return;
  }
  if(!isDelete) {
    return;
  }
  // 代码...
}
~~~

## TypeScript 规范

### 类型定义
- 类型丢失率减少至百分之35%以下，减少使用any定义类型。
- 尽量使用 TypeScript 的类型推测，减少多余类型的定义，除非是复合类型。
~~~typescript
// 正确
const count = 123
const count:number|string = 123
methods: {
	onClick(){/*...*/},
	onMove(){/*...*/},
}
// 不正确
const count:number = 123
methods: {
	onClick():any{/*...*/},
	onMove():any{/*...*/},
}
~~~

### 第三方库
- 优先使用第三方库提供的类型，尽量不要覆盖第三方库类型。
~~~js
import dayjs from "dayjs"
// 正确
const date = dayjs()
// 不正确
const date:any = dayjs()
~~~

### 全局类型
全局数据类型，使用 declare 关键字定义在 src/types/custom.d.ts 中复用。
~~~typescript
// src/types/custom.d.ts
/** 全局接口: 用户数据 */
declare interface UserInfo {
	//....
}
~~~

### 请求参数
请求参数，类型定义是必须的
~~~typescript
// 错误
const getUserInfo = (data:any) => {
  return http.post('/login', data);
};
// 正确
const getUserInfo = (phone:number, code:number) => {
  return http.post('/login', { phone, code });
};

// 错误
const getUserInfo = (params:any) => {
  return http.get('/list', {params});
};
// 正确
// src/types/custom.d.ts
interface ListOpts {
  page: number;
  limit: number;
}
// api/index.ts
const getList = (params:ListOpts) => {
  return http.get('/list', {params});
};
~~~

### 响应参数
当响应参数过于混乱，可不进行定义类型
~~~typescript
// 正确
const getUserInfo = () => {
  return http.post<UserInfo>('/login');
};
// 正确
const getDetails = () => {
  return http.post('/details');
};
~~~