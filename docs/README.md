---
home: true
heroText: Front-End Coding Guidelines
heroImage: /hero.jpg
tagline: null
actionText: 快速上手 →
actionLink: /guide/
features:
- title: HTML 规范
  details: 基于凹凸实验室官方文档，并结合团队日常业务需求以及团队在日常开发过程中总结提炼出的经验而约定。
- title: CSS 规范
  details: 统一团队 CSS 代码书写和 SASS 预编译语言的语法风格，并从业务层面统一规范常用模块的引用。
- title: JavaScript 规范
  details: 统一团队的 JS 语法风格和书写习惯，减少程序出错的概率，其中也包含了 ES6 的语法规范和最佳实践。
- title: TypeScript 规范
  details: 统一团队的 TS 类型的定义，第三方库的使用，请求与响应参数的约定。
- title: 知识扩展
  details: 用于新同事如何快速切入项目，项目开发技术的学习路线。
- title: 常见问题
  details: 总结日常开发出现的疑难杂症，避免二次踩坑。
---