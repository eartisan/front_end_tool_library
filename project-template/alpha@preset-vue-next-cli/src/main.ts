import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import { store as reactiveStore, storeSymbol } from "@/store-reactive";
createApp(App)
  .use(store)
  .use(router)
  .provide(storeSymbol, reactiveStore)
  .mount("#app");
