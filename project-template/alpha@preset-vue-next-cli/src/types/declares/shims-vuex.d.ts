import { InjectionKey } from "vue";
// 覆盖原有 useStore 函数中, 泛型默认值类型
declare module "vuex" {
  // store.comit
  export interface Commit {
    (type: CommitKeys, payload?: any, options?: CommitOptions): void;
    <P extends Payload>(payloadWithType: P, options?: CommitOptions): void;
  }
  // store.dispatch
  export interface Dispatch {
    (type: DispatchKeys, payload?: any, options?: DispatchOptions): Promise<
      any
    >;
    <P extends Payload>(payloadWithType: P, options?: DispatchOptions): Promise<
      any
    >;
  }
  // useStore
  export function useStore<S = StoreStateType & ModulesType>(
    injectKey?: InjectionKey<Store<S>> | string
  ): Store<S>;
  // createStore
  export function createStore<S>(
    options: StoreOptions<S>
  ): Store<S & ModulesType>;
}
