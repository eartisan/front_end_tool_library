/* 自定义全局类型存放 */
type WillObject = {
  [key: string | symbol | number]: any;
};
