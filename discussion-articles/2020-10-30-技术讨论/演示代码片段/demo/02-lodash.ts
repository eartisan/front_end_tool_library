import { chunk, cloneDeep, drop, isArray, isNumber, isObject, random, throttle } from "lodash"

// 将数组拆分多个 size 长度的区块
chunk(['1', '2', '3', '4'], 2) //?

// 深度克隆(数组/对象)
const objects = [{ 'a': 1 }, { 'b': 2 }];
const deepObjects = cloneDeep(objects)
objects[0] === deepObjects[0] //?

// 判断常见类型(数组)
isArray("") //?
isArray([]) //?
isObject("") //?
isObject({}) //?
isNumber('1') //?
isNumber(1) //?

// 函数节流
let count = 0
const fuc = () => console.log(++count)
const throttleFuc = throttle(fuc, 2000, {trailing:false})

// 随机数
Math.floor(Math.random()* 1000) //?
random(0, 15) //?
random(0, 15, true) //?

// 去除数组n个元素
const array = [0, 1, 2, 3]
drop(array, 1) //?