import dayjs from "dayjs"
import objectSupport from "dayjs/plugin/objectSupport"
dayjs.extend(objectSupport)
// 获取当前时间
dayjs() //?
dayjs(new Date()) //?

// 解析时间(指定格式时间)
dayjs("1995-12-25", "YYYY-MM-DD") //?
dayjs("25-12-2001", "DD-MM-YYYY") //?

// 解析时间(时间戳解析毫秒/秒)
dayjs(1318781876406) //?
dayjs.unix(1318781876) //?

// 获取指定时间
dayjs(1318781876406).get("year") //?
dayjs(1318781876406).get("month") //?
dayjs(1318781876406).get("day") //?

// 格式化时间
dayjs().format("当前时间: YYYY年MM月DD日") //?
dayjs().format("剩余时间: hh:mm:ss") //?

// 利用定时器做剩余时间的计算
let time = 1318781876
time--;
dayjs.unix(time).format("当前剩余时间: hh:mm:ss") //?
time--;
dayjs.unix(time).format("当前剩余时间: hh:mm:ss") //?
time--;
dayjs.unix(time).format("当前剩余时间: hh:mm:ss") //?
time--;
dayjs.unix(time).format("当前剩余时间: hh:mm:ss") //?
time--;
dayjs.unix(time).format("当前剩余时间: hh:mm:ss") //?
