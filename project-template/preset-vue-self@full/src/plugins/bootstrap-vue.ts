import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import Vue from 'vue';

import '../custom.scss';

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
