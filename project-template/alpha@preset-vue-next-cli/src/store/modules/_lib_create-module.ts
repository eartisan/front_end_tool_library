import { Store, Module, ModulesType, StoreStateType } from "vuex";
export default <S, R = StoreStateType & ModulesType>(options: Module<S, R>) =>
  options as Store<S>;
