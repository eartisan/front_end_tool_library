// 1. 暴露接口声明
declare interface UserInfo {
  name: string;
  age: number;
  phone: string;
}

// 2. 暴露类型声明
declare type AnyObj = { [key: string]: any }

// 3. 暴露变量声明
declare const props: {
  meg: string;
}

// 4. 暴露任意声明
declare class Store {
  getStore(name:string): any;
  setStore: (name:string, val:any) => void;
}